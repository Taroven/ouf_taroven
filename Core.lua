local _,ns = ...
oUF_Taroven = ns
ns.units = {}
local oUF = ns.oUF
local c = ns.config
local util = ns.util

local solid = 'Interface\\AddOns\\oUF_Taroven\\media\\solid'

local ClassSpells = {
	PRIEST = {
		AuraList = {
			[194384] = { -- Atonement
				indicator = 'bar',
				bar = 1,
				size = 3,
				color = {1,1,0,1},
			},
			[139] = { -- Renew
				indicator = 'bar',
				bar = 2,
				size = 2,
				color = {1,1,0,1},
			},
			[41635] = { -- Prayer of Mending
				indicator = 'stacks',
				location = 'TOPLEFT',
				size = 16,
				alpha = 1,
				offset = -2,
			},
		}
	},
	DRUID = {
		AuraList = {
			[774] = { -- Rejuvenation
				indicator = 'pip',
				location = "TOPLEFT",
				size = 6,
				color = {0,1,0,1},
				offset = 1,
				alpha = 1,
				--[[
				indicator = 'bar',
				bar = 1,
				size = 3,
				color = {0,1,0,1},
				--]]
			},
			[155777] = { -- Rejuvenation (Germination)
				indicator = 'pip',
				location = "BOTTOMLEFT",
				size = 6,
				color = {1,1,0,1},
				offset = 1,
				alpha = 1,
				--[[
				indicator = 'bar',
				bar = 2,
				size = 3,
				color = {1,1,0,1},
				--]]
			},
			[33763] = { -- Lifebloom
				indicator = 'pip',
				location = "BOTTOMRIGHT",
				size = 6,
				color = {1,0,1,1},
				offset = 1,
				alpha = 1,
				--[[
				indicator = 'bar',
				bar = 3,
				size = 3,
				color = {1,0,1,1},
				--]]
			},
			[48438] = { -- Wild Growth
				indicator = 'pip',
				location = "TOPRIGHT",
				size = 6,
				color = {0,0.5,1,1},
				offset = 1,
				alpha = 1,
			},
			--[[
			[93402] = { -- Sunfire

			},
			[8921] = { -- Moonfire

			}, --]]
		},
	},
	MONK = {
		AuraList = {
			[119611] = {
				indicator = 'pip',
				location = "TOPRIGHT",
				size = 4,
				color = {0,1,0,0.6},
				offset = 1,
				alpha = 1,
			}
		}
	},
	WARRIOR = {
		AuraList = {
			[190456] = {
				indicator = 'shield',
				color = {0.5,0.5,1,1},
				size = 6,
				anchor = 'BOTTOMRIGHT',
				alpha = 0.5,
			}
		}
	},
}
ClassSpells[ns.vars.class] = ClassSpells[ns.vars.class] or {}

local getEffectiveUnit = function (self, unit)
	local party = self:GetName():match('_party') -- Fixed: GetParent() returns oUF_PetBattleFrameHider, which is definitely not what we want
	local raid = self:GetName():match('_raid')
	local pet = self:GetName():match('pet')

	if party and pet then return 'partypet'
	elseif party then return 'party'
	elseif raid then return 'raid'
	else return unit end
end

for k,v in pairs(ns.tags) do
	oUF.Tags.Methods[k] = v.func
	oUF.Tags.Events[k] = v.event
end

-- Threat and debuff situation funcs
local GetDebuffs, updateSituation
local wipe = wipe or table.wipe
do
	GetDebuffs = function (unit)
		if not UnitCanAssist('player',unit) then return nil end
		for i = 1,100 do
			local name,icon,count,dispelType,duration,expires,caster,_,_,spellID= UnitDebuff(unit,i,'RAID')
			if dispelType then return dispelType end
			if not (name and texture) then break end
		end
	end

	updateSituation = function (self,event,u)
		if not u or self.unit ~= u then return end
		local a = c.Visuals.Alpha
		local m = c.Visuals.ColorMult or 1
		local mb = c.Visuals.ColorMultBorder or 1
		local debufftype = GetDebuffs(u)
		local s = UnitThreatSituation(u)
		if debufftype then
			local color = DebuffTypeColor[debufftype] 
			self.Glow:SetBackdropBorderColor(color.r * mb, color.g * mb, color.b * mb, a.glow.warn)
			self.Health:SetStatusBarColor(color.r * m, color.g * m, color.b * m, a.bar.warn)
		elseif s and s > 1 then
			r,g,b = GetThreatStatusColor(s)
			self.Glow:SetBackdropBorderColor(r,g,b, a.glow.warn)
			self.Health:SetStatusBarColor(0,0,0, a.bar.low)
		else
			self.Glow:SetBackdropBorderColor(0,0,0, a.glow.low)
			self.Health:SetStatusBarColor(0,0,0, a.bar.low)
		end
	end
end

local updateHealth = function(self,unit,min,max)
	if not unit then return end
	local r,g,b,t
	local isdead = UnitIsDeadOrGhost(unit)
	
	if isdead or not UnitIsConnected(unit) then
		r,g,b = .6,.6,.6
	elseif(UnitIsPlayer(unit)) then
		local _,class = UnitClass(unit)
		t = oUF.colors.class[class]
	else
		r,g,b = .2,.5,.5
	end

	if(t) then
		r,g,b = t[1],t[2],t[3]
	end

	--print('updateHealth', r, g, b)

	self.bg:SetVertexColor(r,g,b,c.Visuals.Alpha.bar.bg)
end

local createHealth = function (self, effectiveUnit)
	self.Health = CreateFrame('StatusBar',self:GetName()..'_health',self)
	self.Health.tex = self.Health:CreateTexture(nil,'BORDER',nil,0)
	self.Health.tex:SetAllPoints(self.Health)
	util.SetTextureByConfig(self.Health.tex, c.Visuals.Textures.bar)
	self.Health:SetStatusBarTexture(self.Health.tex)
	self.Health:SetStatusBarColor(c.Color.Health[1],c.Color.Health[2], c.Color.Health[3], c.Visuals.Alpha.bar.low)

	self.Health.bg = self.Health:CreateTexture(nil,'BACKGROUND')
	self.Health.bg:SetAllPoints(self.Health)
	util.SetTextureByConfig(self.Health.bg, c.Visuals.Textures.bar)

	self.Health:SetPoint('TOPLEFT', self, 'TOPLEFT')
	if effectiveUnit == 'raid' then
		self.Health:SetPoint('BOTTOMRIGHT', self.Power, 'BOTTOMLEFT')
	else
		self.Health:SetPoint('BOTTOMRIGHT', self.Power, 'TOPRIGHT')
	end

	self.Health.PostUpdate = updateHealth
end

local createPower = function (self, effectiveUnit)
	self.Power = CreateFrame('StatusBar',self:GetName()..'_power',self)
	self.Power.tex = self.Power:CreateTexture(nil,'ARTWORK',nil,0)
	self.Power.tex:SetAllPoints(self.Power)
	util.SetTextureByConfig(self.Power.tex, c.Visuals.Textures.power)
	self.Power:SetStatusBarTexture(self.Power.tex)

	self.Power.colorClass = true
	self.Power.colorTapping = true
	self.Power.colorDisconnected = true
	self.Power.colorReaction = true
	
	if effectiveUnit == 'raid' then
		self.Power:SetSize(c.Dimensions.power, c.Dimensions.raid.size)
		self.Power:SetPoint('TOPRIGHT', self, 'TOPRIGHT')
	elseif c.Dimensions[effectiveUnit] then
		self.Power:SetSize(c.Dimensions[effectiveUnit].width, c.Dimensions.power)
		self.Power:SetPoint('BOTTOMLEFT', self, 'BOTTOMLEFT')
	else
		self.Power:SetSize(c.Dimensions.Main.width, c.Dimensions.power)
		self.Power:SetPoint('BOTTOMLEFT', self, 'BOTTOMLEFT')
	end

	self.Power.bg = self.Power:CreateTexture(nil,'BACKGROUND')
	util.SetTextureByConfig(self.Power.bg, c.Visuals.Textures.power)
	self.Power.bg:SetAllPoints(self.Power)
	self.Power.bg.multiplier = 0.3
	
	self.Power:SetAlpha(c.Visuals.Alpha.bar.warn)
	self.Power.frequentUpdates = true
end

local createGlow = function (self)
	self.Glow = CreateFrame('Frame',nil,self)
	self.Glow:SetPoint('TOPLEFT',self.Health,'TOPLEFT',-4,4)
	self.Glow:SetPoint('BOTTOMRIGHT',self.Power,'BOTTOMRIGHT',4,-4)
	self.Glow:SetFrameStrata('BACKGROUND')
	self.Glow:SetBackdrop {
		edgeFile = c.Visuals.Textures.glow, edgeSize = 5,
		insets = {left = 3,right = 3,top = 3,bottom = 3}
	}
	self.Glow:SetBackdropColor(0,0,0,0)
	self.Glow:SetBackdropBorderColor(0,0,0, c.Visuals.Alpha.glow.low)
	self.Glow.bar = self.Health
end

local createHighlight = function (self)
	self.Highlight = self.Health:CreateTexture(nil, "OVERLAY")
	self.Highlight:SetAllPoints(self.Health)
	util.SetTextureByConfig(self.Highlight, c.Visuals.Textures.bar)
	self.Highlight:SetVertexColor(unpack(c.Color.Highlight))
	self.Highlight:SetBlendMode('ADD')
	self.Highlight:Hide()
end

local createCast = function (self)
	self.Castbar = CreateFrame('StatusBar',nil,self)
	--self.Castbar:SetFrameStrata('HIGH')
	local player = self:GetName():match('oUF_TarovenPlayer')
	local target = self:GetName():match('oUF_Target')
	
	self.Castbar.tex = self.Castbar:CreateTexture(nil, 'ARTWORK', nil, 3)
	util.SetTextureByConfig(self.Castbar.tex, c.Visuals.Textures.bar)

	self.Castbar.tex:SetVertexColor(unpack(c.Color.Castbar))
	self.Castbar.tex:SetBlendMode('ADD')
	
	if player then
		self.Castbar.SafeZone = self.Castbar:CreateTexture(nil, 'ARTWORK', nil, 4)
		self.Castbar.SafeZone:SetColorTexture(0.25,0.5,0)
		self.Castbar.SafeZone:SetAlpha(0.35)
		self.Castbar.SafeZone:SetBlendMode('ADD')
	end
	
	self.Castbar.spark = self.Castbar:CreateTexture(nil, 'ARTWORK', nil, 5)
	self.Castbar.spark:SetColorTexture(1,1,1)
	self.Castbar.spark:SetVertexColor(unpack(c.Color.CastbarSpark))
	self.Castbar.spark:SetWidth(1)
	self.Castbar.spark:SetPoint('TOPRIGHT',self.Castbar.tex,'TOPRIGHT')
	self.Castbar.spark:SetPoint('BOTTOMRIGHT',self.Castbar.tex,'BOTTOMRIGHT')
	
	self.Castbar:SetStatusBarTexture(self.Castbar.tex)

	self.Castbar:SetAllPoints(self.Health)
	self.Castbar.tex:SetAlpha(0.25)

--[[ -- Issue: Castbar shield doesn't respect SetSize. Need a custom graphic or other workaround
	if target then
		local size = self.Health:GetHeight()
		self.Castbar.Shield = self.Castbar:CreateTexture(nil, "OVERLAY")
		self.Castbar.Shield:SetTexture(c.Visuals.Textures.shield)
		self.Castbar.Shield:SetVertexColor(1,0,0,0.5)
		self.Castbar.Shield:SetSize(size,size)
		self.Castbar.Shield:SetPoint('CENTER', 0, 0)
	end
--]]

	if player or target then
		self.Castbar.Text = self.Castbar:CreateFontString(nil,'OVERLAY')
		--self.Castbar.Text:SetAllPoints(self.Health)
		self.Castbar.Text:SetPoint('BOTTOM', self, 'TOP', 0, 5)
		self.Castbar.Text:SetFont(c.Visuals.Font, c.Visuals.FontSizeHuge, c.Visuals.FontFlag)
		self.Castbar.Text:SetShadowColor(unpack(c.Color.FontShadow))
		self.Castbar.Text:SetShadowOffset(c.Visuals.FontShadowSmallOffset, -c.Visuals.FontShadowSmallOffset)
		self.Castbar.Text:SetAlpha(0.9)
	end
end

local anchors = {
	Buffs = {
		anchor = "TOPLEFT",
		point = {'TOPLEFT', 'BOTTOMLEFT', 0, -3},
		size = c.Dimensions.aurasize,
		growX = 'RIGHT',
		growY = 'DOWN',
		initialAnchor = 'TOPLEFT',
		width = c.Dimensions.Main.width / 2,
		height = c.Dimensions.aurasize * 3,
	},
		
	Debuffs = {
		anchor = "TOPRIGHT",
		point = {'TOPRIGHT', 'BOTTOMRIGHT', 0, -3},
		size = c.Dimensions.aurasize,
		growX = 'LEFT',
		growY = 'DOWN',
		initialAnchor = 'TOPRIGHT',
		width = c.Dimensions.Main.width / 2,
		filter = 'HARMFUL|PLAYER',
	},
}

local createAuras = function (self,effectiveUnit)
	if effectiveUnit == 'target' then

		
		do
			local a = anchors.Debuffs
			a.num = a.width / a.size
			a.padding = (a.width-(a.size*a.num))/a.num
			
			if a.padding < 1 then
				while a.padding < 1 do
					a.num = a.num-1
					a.padding = (a.width-(a.size*a.num-1))/a.num
				end
			end

			self.Debuffs = CreateFrame('Frame',nil,self)
			self.Debuffs["growth-y"] = a.growY
			self.Debuffs["growth-x"] = a.growX
			self.Debuffs:SetPoint(a.point[1], self, a.point[2], a.point[3], a.point[4])
			self.Debuffs.initialAnchor = a.initialAnchor
			self.Debuffs.size = a.size
			self.Debuffs:SetHeight(a.height or a.size)
			self.Debuffs:SetWidth(a.width or self.vars.width)
			self.Debuffs.num = a.num
			self.Debuffs.spacing = a.padding
			self.Debuffs.filter = a.filter
		end

		do
			local a = anchors.Buffs
			a.num = a.width / a.size
			a.padding = (a.width-(a.size*a.num))/a.num
			
			if a.padding < 1 then
				while a.padding < 1 do
					a.num = a.num-1
					a.padding = (a.width-(a.size*a.num-1))/a.num
				end
			end
			self.Buffs = CreateFrame('Frame',nil,self)
			self.Buffs["growth-y"] = a.growY
			self.Buffs["growth-x"] = a.growX
			self.Buffs:SetPoint(a.point[1], self, a.point[2], a.point[3], a.point[4])
			self.Buffs.initialAnchor = a.initialAnchor
			self.Buffs.size = a.size
			self.Buffs:SetHeight(a.height or a.size)
			self.Buffs:SetWidth(a.width or self.vars.width)
			self.Buffs.num = a.num
			self.Buffs.spacing = a.padding
			self.Buffs.filter = a.filter
		end
	end
end

local secondaryPower = {
	ROGUE = '[T:cpoints]',
	MONK = '[chi]',
	WARLOCK = '[soulshards]',
	PALADIN = '[holypower]',
	PRIEST = '[T:specialpowerperc]',
	DRUID = '[T:specialpowerperc][T:cpoints]',
}
local createSecondaryPower = function (self, effectiveUnit)
	if (effectiveUnit == 'player') and secondaryPower[ns.vars.class] then		
		local cp = self.Health:CreateFontString(nil,'OVERLAY')
		cp:SetFont(c.Visuals.Font, c.Visuals.FontSizeGiant, c.Visuals.FontFlag)
		cp:SetShadowColor(unpack(c.Color.FontShadow))
		cp:SetShadowOffset(c.Visuals.FontShadowSmallOffset, -c.Visuals.FontShadowSmallOffset)
		cp:SetAlpha(c.Visuals.Alpha.HealthText.name)
		cp:SetPoint('CENTER', 0, 2)
		cp:SetJustifyH('CENTER')
		
		self.SecondaryPower = cp
		self:Tag(self.SecondaryPower, '[raidcolor]'..secondaryPower[ns.vars.class])
	end
end

local getPipOffset = function (corner, offset)
	if corner == 'TOPLEFT' then return offset, -offset
	elseif corner == 'LEFT' then return offset, 0
	elseif corner == 'BOTTOMLEFT' then return offset, offset
	elseif corner == 'BOTTOM' then return 0, offset
	elseif corner == 'BOTTOMRIGHT' then return -offset, offset
	elseif corner == 'RIGHT' then return -offset, 0
	elseif corner == 'TOPRIGHT' then return -offset, -offset
	elseif corner == 'TOP' then return 0, -offset
	end
end

local createIndicators = function (self)
	local ai = ClassSpells[ns.vars.class]
	if ai and ai.AuraList then
		local sai = { Frames = {}, AuraList = ai.AuraList }
		for id,cfg in pairs(ai.AuraList) do
			if cfg.indicator == 'bar' then
				local b = CreateFrame('StatusBar',nil,self.Health)
				b:SetPoint('BOTTOMLEFT', self.Health, 'BOTTOMLEFT', 0, cfg.size * (cfg.bar - 1) + (cfg.bar > 1 and 1 or 0))
				b:SetFrameStrata('HIGH')
				b:SetSize(self.Health:GetWidth(), cfg.size)
				b.tex = b:CreateTexture(nil,'OVERLAY',nil,6)
				b.tex:SetTexture(c.Visuals.Textures.bar)
				--b.tex:SetColorTexture(1,1,1,0.3)
				b.tex:SetVertexColor(unpack(cfg.color))
				b.tex:SetBlendMode('ADD')
				b.tex:SetAllPoints(b)
				b:SetStatusBarTexture(b.tex)
				b:SetMinMaxValues(0,1)
				b:Hide()
				sai.Frames[cfg.bar] = b
			elseif cfg.indicator == 'shield' then
				local b = CreateFrame('StatusBar',nil,self.Health)
				local origin = self.vars.vertical and 'BOTTOM' or 'LEFT'
				local orientation = self.vars.vertical and 'VERTICAL' or 'HORIZONTAL'
				local xsize = (self.vars.vertical and (cfg.size or self.vars.Health.width)) or self.vars.Health.width
				local ysize = (self.vars.vertical and self.vars.Health.height) or (cfg.size or self.vars.Health.height)
				b:SetPoint('TOPRIGHT', self.Health:GetStatusBarTexture(), 'TOPRIGHT')
				b:SetPoint(origin, self.Health:GetStatusBarTexture(), origin)
				b:SetSize(xsize, ysize)
				b:SetReverseFill(true)
				b.alpha = cfg.alpha or 1
				b.tex = b:CreateTexture(nil,'OVERLAY',nil,6)
				b.tex:SetTexture(c.Visuals.Textures.bar)
				--b.tex:SetColorTexture(1,1,1,1)
				b.tex:SetVertexColor(unpack(cfg.color))
				b.tex:SetBlendMode('ADD')
				b.tex:SetAllPoints(b)
				b:SetStatusBarTexture(b.tex)
				b:Show()
				b:SetAlpha(0)
				sai.Frames.shield = b
			else
				local x,y = getPipOffset(cfg.location, cfg.offset)
				local f
				if cfg.indicator == 'stacks' then
					f = self.Health:CreateFontString(nil,'OVERLAY')
					f:SetFont(c.Visuals.Font, c.Visuals.FontSizeLarge, c.Visuals.FontFlag)
					f:SetShadowColor(unpack(c.Color.FontShadow))
					f:SetShadowOffset(c.Visuals.FontShadowSmallOffset, -c.Visuals.FontShadowSmallOffset)
					f:SetAlpha(cfg.alpha)
					f:SetSize(cfg.size,cfg.size)
					f:SetJustifyH(string.match(cfg.location, "LEFT") or string.match(cfg.location, "RIGHT") or 'CENTER')
					f:SetPoint(cfg.location, self, cfg.location, x, y)
				else
					f = self.Health:CreateTexture(nil,'OVERLAY')
					--f:SetTexture(c.Visuals.Textures.bar)
					f:SetColorTexture(1,1,1,1)
					f:SetVertexColor(unpack(cfg.color))
					f:SetBlendMode('ADD')
					f:SetSize(cfg.size, cfg.size)
					f:SetPoint(cfg.location, self.Health, cfg.location, x, y)
				end
				sai.Frames[cfg.location] = f
			end
		end
		self.AuraIndicators = sai
	end
end

local OnEnter = function(self)
	UnitFrame_OnEnter(self)
	self.Highlight:Show()	
end

local OnLeave = function(self)
	UnitFrame_OnLeave(self)
	self.Highlight:Hide()	
end

local UnitSpecific = {
	'player',
--	'pet',
--	'pettarget',
	'target', -- 'targettarget'
--	'focus', -- 'focustarget'
}

local Shared = function (self, unit)
	local eu = getEffectiveUnit(self, unit)
	
--	print(self:GetParent():GetName(), self:GetName(), eu) -- DEBUG

	if c.Dimensions[eu] then
	--	print('custom EU', eu)
		self:SetSize(c.Dimensions[eu].size or c.Dimensions[eu].width, c.Dimensions[eu].size or c.Dimensions[eu].height)
	else
		self:SetSize(c.Dimensions.Main.width, c.Dimensions.Main.height)
	end

	createPower(self,unit,eu)
	createHealth(self,unit,eu)
	createGlow(self)
	createHighlight(self)
	createCast(self)
	createAuras(self,eu)
	createSecondaryPower(self,eu)
	createIndicators(self) -- FIXME: Bars don't work. Pips are fine. Shields untested.

	if eu == 'raid' then
		self.Health:SetOrientation("VERTICAL")
		self.Power:SetOrientation("VERTICAL")
	end

	self:SetScript("OnEnter", OnEnter)
	self:SetScript("OnLeave", OnLeave)
	self:RegisterEvent('UNIT_THREAT_LIST_UPDATE',updateSituation)
	self:RegisterEvent('UNIT_THREAT_SITUATION_UPDATE',updateSituation)
	self:RegisterEvent('UNIT_AURA',updateSituation)
	self:RegisterEvent('UNIT_TARGET',updateSituation)
end

oUF:RegisterStyle('Taroven', Shared)
oUF:RegisterStyle('Taroven - Party', Shared)
oUF:RegisterStyle('Taroven - Raid', Shared)

oUF:Factory(function(self)
	self:SetActiveStyle('Taroven')
	self:Spawn('player'):SetPoint('CENTER', c.Dimensions.positionX, c.Dimensions.positionY)
	self:Spawn('target'):SetPoint('CENTER', -c.Dimensions.positionX, c.Dimensions.positionY)
	self:SetActiveStyle('Taroven - Party')
	local party = oUF:SpawnHeader('oUF_Taroven_party', nil, "custom [group:party,nogroup:raid][@raid6,noexists,group:raid] show;hide",
	--local party = oUF:SpawnHeader('oUF_Taroven_party', nil, "custom [group:party,nogroup:raid] show;hide",
	--local party = oUF:SpawnHeader('oUF_Taroven_party', nil, "custom show", -- DEBUG
		'showParty',	true,
		'showPlayer',	false, -- DEBUG
		'showSolo',		false, -- DEBUG
		'point',		'TOP',
		'xOffset',		0,
		'yOffset',		-5,
		'oUF-initialConfigFunction', ([[
		self:SetWidth(%d)
		self:SetHeight(%d)
		]]):format(c.Dimensions.party.width, c.Dimensions.party.height))
	party:SetPoint('TOPRIGHT', oUF_TarovenPlayer, 'BOTTOMRIGHT', 0, -5)

	self:SetActiveStyle('Taroven - Raid')
	--local raid = oUF:SpawnHeader("oUF_Raid", nil, "solo,party,raid10,raid25,raid40",
	--local raid = oUF:SpawnHeader("oUF_Raid", nil, "solo,party,raid,raid10,raid25,raid40",
	local raid = oUF:SpawnHeader("oUF_Raid", nil, "custom [@raid6,exists,group:raid][@raid7,exists,group:raid][@raid8,exists,group:raid][@raid9,exists,group:raid] show;hide",
		'showRaid',				true,
		'showPlayer',			true,
		'showSolo',				false, -- DEBUG
		'showParty',			false, -- DEBUG
		'xOffset',				-c.Dimensions.raid.padding,
		'yOffset',				0,
		'groupFilter',			'1,2,3,4,5',
		'groupBy',				'ROLE',
		'groupingOrder',		'TANK, HEALER, DAMAGER, NONE',
		--'sortMethod',	'NAME',
		'maxColumns',			5,
		'unitsPerColumn',		5,
		'columnSpacing',		c.Dimensions.raid.padding,
		'point',				'RIGHT',
		'columnAnchorPoint',	'TOP',
		'oUF-initialConfigFunction', ([[
		self:SetWidth(%d)
		self:SetHeight(%d)
		]]):format(c.Dimensions.raid.size, c.Dimensions.raid.size))
	raid:SetPoint('TOPRIGHT', oUF_TarovenPlayer, 'BOTTOMRIGHT', 0, -5)
end)