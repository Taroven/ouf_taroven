local _,ns = ...

local gsub = string.gsub
local format = string.format
local floor = math.floor

local c = ns.config
ns.vars = {
	class = select(2,UnitClass('player')),
}
local util = {}
ns.util = util

util.menu = function (self)
	local unit = gsub(self.unit,'(.)',string.upper,1)
	if(_G[unit..'FrameDropDown']) then
		ToggleDropDownMenu(1,nil,_G[unit..'FrameDropDown'],'cursor')
	end
end

util.truncate = function (value)
	if(value >= 1e6) then
		return gsub(format('%.1fm',value / 1e6),'%.?0+([km])$','%1')
	elseif(value >= 1e5) then
		return gsub(format('%.0fk',value / 1e3),'%.?0+([km])&','%1')
	elseif(value >= 1e4) then
		return gsub(format('%.1fk',value / 1e3),'%.?0+([km])$','%1')
	else
		return value
	end
end

util.Round = function (num,idp)
  if idp and idp>0 then
	local mult = 10^idp
	return floor(num * mult + 0.5) / mult
  end
  return floor(num + 0.5)
end

util.hex = function (r,g,b)
	local r,g,b = r,g,b
	if(type(r) == 'table') then
		if(r.r) then r,g,b = r.r,r.g,r.b else r,g,b = unpack(r) end
	end
	if not r then r,g,b = .5,.5,.5 end
	return string.format('|cff%02x%02x%02x',r * 255,g * 255,b * 255)
end

-- Borrowed from Freebaser - Usage: string, beginning letter, length (UnitName, 1, 4)
util.utf8sub = function (str, start, numChars) 
	local currentIndex = start 
	while numChars > 0 and currentIndex <= #str do 
		local char = string.byte(str, currentIndex) 
		if char >= 240 then 
		  currentIndex = currentIndex + 4 
		elseif char >= 225 then 
		  currentIndex = currentIndex + 3 
		elseif char >= 192 then 
		  currentIndex = currentIndex + 2 
		else 
		  currentIndex = currentIndex + 1 
		end 
		numChars = numChars - 1 
	end 
	return str:sub(start, currentIndex - 1) 
end

util.CalcAuras = function (frame, t)
	local aura = frame.vars[t]; if not aura then return end
	
	aura.num = (t.width or frame.vars.width)/aura.size
	aura.padding = ((t.width or frame.vars.width)-(aura.size*aura.num))/aura.num
	
	if aura.padding < 1 then
		while aura.padding < 1 do
			aura.num = aura.num-1
			aura.padding = ((t.width or frame.vars.width)-(aura.size*aura.num-1))/aura.num
		end
	end
	
	return true
end

util.CreateFS = function (self, font, fontSize, justify, offset, tag)
	local fs = self:CreateFontString(nil, 'OVERLAY')
	fs:SetFont(font or c.Visuals.Font, fontSize or c.Visuals.FontSize)
	fs:SetShadowColor(unpack(c.Color.FontShadow))
	fs:SetShadowOffset(offset or c.Visuals.FontShadowOffset, (offset and -offset) or -c.Visuals.FontShadowOffset)
	if justify then
		fs:SetJustifyH(justify)
	end
	if tag then
		self:Tag(fs, tag)
	end
	return fs
end

util.CreateStack = function (self, baseType, text, overlay)
	local f = CreateFrame(baseType or 'Frame', nil, self or UIParent)
	if type(text) == 'table' then
		f.Text = util.CreateFS(unpack(text))
	elseif text then
		f.Text = f:CreateFontString(nil, 'OVERLAY')
	end

	if overlay then
		f.Overlay = CreateFrame('Frame', nil, f)
		f.Overlay:SetFrameLevel(f:GetFrameLevel() + 1)
	end

	return f
end

util.SetTextureByConfig = function (self, texture)
	if c.Visuals.IgnoreBarTextures then
		return self:SetColorTexture(1,1,1)
	else
		return self:SetTexture(texture)
	end
end