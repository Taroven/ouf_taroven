local _,ns = ...
local util = ns.util

local powerFunc = function (u, pType)
	local oUF = ns.oUF or oUF
	local r,g,b,t
	
	if isdead or not UnitIsConnected(u) then
		r,g,b = .6,.6,.6
	elseif(UnitIsPlayer(u)) then
		local _,class = UnitClass(u)
		t = oUF.colors.class[class]
	else
		r,g,b = .2,.5,.5
	end

	if(t) then
		r,g,b = t[1],t[2],t[3]
	end
	local power = UnitPower(u,pType)

	return util.hex(r,g,b)..util.truncate(power, 0)
end

local powerFuncPerc = function (u, pType)
	local oUF = ns.oUF or oUF
	local r,g,b,t
	
	if isdead or not UnitIsConnected(u) then
		r,g,b = .6,.6,.6
	elseif(UnitIsPlayer(u)) then
		local _,class = UnitClass(u)
		t = oUF.colors.class[class]
	else
		r,g,b = .2,.5,.5
	end

	if(t) then
		r,g,b = t[1],t[2],t[3]
	end
	local power = UnitPower(u,pType)
	local pMax = UnitPowerMax(u,pType)
	local perc = util.Round((power / pMax) * 100)

	return util.hex(r,g,b)..perc
end

ns.tags = {
	['T:namelong'] = {
		event = 'UNIT_HEALTH_FREQUENT',
		func = function (u)
			local oUF = ns.oUF or oUF
			local r,g,b,t
			local min,max = UnitHealth(u),UnitHealthMax(u)
			local isdead = UnitIsDeadOrGhost(u)
			
			if isdead or not UnitIsConnected(u) then
				r,g,b = .6,.6,.6
			elseif(UnitIsPlayer(u)) then
				local _,class = UnitClass(u)
				t = oUF.colors.class[class]
			else
				r,g,b = .2,.5,.5
			end

			if(t) then
				r,g,b = t[1],t[2],t[3]
			end
			local per = min/max

			if (per > .95) or isdead then
				return util.hex(r,g,b)..UnitName(u)
			else
				return util.hex(r,g,b)..'-'..util.truncate(max-min)
			end
		end,
	},

	['T:nameshort'] = {
		event = 'UNIT_HEALTH_FREQUENT',
		func = function (u)
			local oUF = ns.oUF or oUF
			local r,g,b,t
			local min,max = UnitHealth(u),UnitHealthMax(u)
			local isdead = UnitIsDeadOrGhost(u)
			
			if isdead or not UnitIsConnected(u) then
				r,g,b = .6,.6,.6
			elseif(UnitIsPlayer(u)) then
				local _,class = UnitClass(u)
				t = oUF.colors.class[class]
			else
				r,g,b = .2,.5,.5
			end

			if(t) then
				r,g,b = t[1],t[2],t[3]
			end
			local per = min/max

			if (per > .95) or isdead then
				return util.hex(r,g,b)..util.utf8sub(UnitName(u),1,4)
			else
				return util.hex(r,g,b)..'-'..util.truncate(max-min)
			end
		end,
	},
	
	['T:namemedium'] = {
		event = 'UNIT_HEALTH_FREQUENT',
		func = function (u)
			local oUF = ns.oUF or oUF
			local r,g,b,t
			local min,max = UnitHealth(u),UnitHealthMax(u)
			local isdead = UnitIsDeadOrGhost(u)
			
			if isdead or not UnitIsConnected(u) then
				r,g,b = .6,.6,.6
			elseif(UnitIsPlayer(u)) then
				local _,class = UnitClass(u)
				t = oUF.colors.class[class]
			else
				r,g,b = .2,.5,.5
			end

			if(t) then
				r,g,b = t[1],t[2],t[3]
			end
			local per = min/max

			if (per > .95) or isdead then
				return util.hex(r,g,b)..util.utf8sub(UnitName(u),1,12)
			else
				return util.hex(r,g,b)..'-'..util.truncate(max-min)
			end
		end,
	},

	['T:specialpower'] = {
		event = 'UNIT_POWER_FREQUENT',
		func = function(u)
			local isSpecial = UnitPowerType(u) >= 4
			if isSpecial then return powerFunc(u, 0) end
		end,
	},

	['T:specialpowerperc'] = {
		event = 'UNIT_POWER_FREQUENT',
		func = function(u)
			local isSpecial = UnitPowerType(u) > 4
			if isSpecial then return powerFuncPerc(u, 0) end
		end,
	},
	
	['T:power'] = {
		event = 'UNIT_POWER_FREQUENT',
		func = powerFunc,
	},

	['T:powerperc'] = {
		event = 'UNIT_POWER_FREQUENT',
		func = powerFuncPerc,
	},

	['T:cpoints'] = {
		event = 'UNIT_POWER_FREQUENT',
		func = function(u)
			local cp
			if(UnitHasVehicleUI'player') then
				cp = GetComboPoints('vehicle', 'target')
			else
				cp = UnitPower(u,4)
			end

			if(cp > 0) then
				return cp
			end
		end,
	},
	
	['T:health'] = {
		event = 'UNIT_HEALTH_FREQUENT',
		func = function (u)
			local oUF = ns.oUF or oUF
			local r,g,b,t
			local min = UnitHealth(u)
			local isdead = UnitIsDeadOrGhost(u)
			
			if isdead or not UnitIsConnected(u) then
				r,g,b = .6,.6,.6
			elseif(UnitIsPlayer(u)) then
				local _,class = UnitClass(u)
				t = oUF.colors.class[class]
			else
				r,g,b = .2,.5,.5
			end

			if(t) then
				r,g,b = t[1],t[2],t[3]
			end

			return util.hex(r,g,b)..util.truncate(min)
		end,
	},
}