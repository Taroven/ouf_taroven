local _,ns = ...
local c = {}
ns.config = c

local mediapath = 'Interface\\AddOns\\oUF_Taroven\\media\\'

c.Dimensions = {
	power = 3, -- health size is automatic, this is a global adjustment for the power bar size
	positionX = -300, -- Position of the player frame. The rest of the frames are based on this.
	positionY = -100,
	aurasize = 20,

	Main = {
		width = 240,
		height = 20,
	},

	party = {
		width = 180,
		height = 18,
	},
}

c.Dimensions.raid = {
	padding = 4,
	powersize = 2,
	aurasize = 2,
}
c.Dimensions.raid.size = ((c.Dimensions.Main.width)-(c.Dimensions.raid.padding*4))/5 -- Probably best not to mess with this unless you know what you're doing, or REALLY want a specific size.


c.Visuals = {
	Textures = {
		border = mediapath..'border',
		bar = mediapath..'solid',
		barBG = mediapath..'solid',
		glow = mediapath..'glowTex',
		power = mediapath..'solid',
		shield = mediapath..'solid',
	},

	IgnoreBarTextures = false, -- Ignore .Textures settings for statusbars and just use solid colors. Recommended to use 'solid' above instead, as there are some minor glitches with this.
	
	--Font = mediapath..'visitor.ttf',
	--Font = mediapath..'Walkway Bold.ttf',
	Font = mediapath..'Walkway Black.ttf',
	FontSize = 12,
	FontSizeRaid = 11,
	FontSizeParty = 15,
	FontSizeLarge = 13,
	FontSizeHuge = 20,
	FontSizeGiant = 20,
	FontSizeStacks = 11,
	FontShadowOffset = 1,
	FontShadowSmallOffset = 1,
	--FontFlag = 'OUTLINE',
	
	PowerMult = 0.3,
	ColorMult = 0.65, -- color multiplier for dispellable debuff warnings
	ColorMultBorder = 1,
	
	Alpha = {
		glow = {
			warn = 0.7,
			low = 0.6,
		},
		bar = {
			warn = 0.9,
			low = 0.65,
			bg = 0.5,
		},
		HealthText = {
			perc = 0.8,
			val = 0.8,
			name = 0.7,
		},
	},
}

c.Color = {	
	Health = {0,0,0},
	Castbar = {1,1,1},
	CastbarSpark = {0,0,0},
	FontShadow = {0,0,0,1},
	Highlight = {1,1,1,0.07},
	MyHeals = {0, 1, 0, 0.25},
	OtherHeals = {0.25, 0.75, 0.25, 0.2},
}