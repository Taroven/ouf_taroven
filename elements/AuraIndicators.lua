local _,ns = ...
local oUF = ns.oUF or oUF

local UnitAura, UnitHealth, UnitHealthMax = UnitAura, UnitHealth, UnitHealthMax

local typeFuncs = {
	pip = function (ai, id, config)
		if ai.Auras[id] then
			ai.Frames[config.location]:Show()
			if not(config.color) then
				if config.harm then
					config.color = {1,0,0,0.9}
				else
					config.color = {0,1,0,0.9}
				end
			end
			ai.Frames[config.location]:SetVertexColor(unpack(config.color))
		else
			ai.Frames[config.location]:Hide()
		end
	end,
	
	bar = function (ai, id, config)
		if ai.Auras[id] then
			ai.Timers[id] = ai.Timers[id] or {}
			local t = ai.Timers[id]
			local a = ai.Auras[id]
			t.stop = a.stop
			t.duration = a.duration
			t.start = a.stop - a.duration
			
			ai.Frames[config.bar]:Show()
			ai.Frames[config.bar]:SetScript('OnUpdate', function (self,elapsed)
				t.current = (t.stop - GetTime())/t.duration
				self:SetValue(t.current)
				if t.current <= 0 then self:SetScript('OnUpdate',nil); self:Hide() end
			end)
		else
			ai.Frames[config.bar]:SetScript('OnUpdate',nil)
			ai.Frames[config.bar]:Hide()
			ai.Timers[id] = nil
		end
	end,
	
	stacks = function (ai, id, config)
		if ai.Auras[id] then
			local c = ai.Auras[id].count
			ai.Frames[config.location]:Show()
			ai.Frames[config.location]:SetText(c)
			if c == 1 then
				if not(config.warning) then config.warning = {1,0,0,1} end
				ai.Frames[config.location]:SetVertexColor(unpack(config.warning))
			elseif c == 2 then
				if not(config.low) then config.low = {0,0,1,0.9} end
				ai.Frames[config.location]:SetVertexColor(unpack(config.low))
			else
				if not(config.good) then config.good = {0,1,0,0.8} end
				ai.Frames[config.location]:SetVertexColor(unpack(config.good))
			end
		else
			ai.Frames[config.location]:SetText()
			ai.Frames[config.location]:Hide()
		end
	end,

	shield = function (ai, id, config)
		if ai.Auras[id] and type(ai.Auras[id].v1) == 'number' and ai.Auras[id].v1 > 0 then
			local a = ai.Auras[id]
			local f = ai.Frames.shield

			f:Show()
			f:SetAlpha(f.alpha)
			f:SetScript('OnUpdate', function (self,elapsed)
				local min = UnitHealth('player')
				local max = UnitHealthMax('player')
				local diff = max - (max - min)
				self:SetMinMaxValues(0, diff)
				self:SetValue(a.v1)
			end)
		else
			ai.Frames.shield:SetScript('OnUpdate',nil)
			ai.Frames.shield:SetAlpha(0)
		end
	end,
}

-- Return the first found instance of an aura on the owner unit. For buffs, only returns those cast by the player.
local GetAuras = function (self, unit, aurafunc, ai, filter)
	if type(aurafunc)~='function' then return end
	for spellid,attrs in pairs(ai.AuraList) do
		local sn = ai.Cache[spellid]
		if not(sn) then
			sn = GetSpellInfo(spellid)
			ai.Cache[spellid] = sn
		end
		for i = 1,40 do
			local name, icon, count, dispelType, duration, expires, caster, isStealable, shouldConsolidate, spellID, canApplyAura, isBossDebuff, _, _, _, v1, v2, v3 = aurafunc(unit, i, filter)
			if name == sn then
				ai.Auras[spellID] = {
					name = name,
					count = count,
					duration = duration,
					stop = expires,
					spellID = spellID,
					v1 = v1,
					v2 = v2,
					v3 = v3
				}
				break
			end
		end
	end
end

local UNIT_AURA = function (self, event, unit)
	if (self.unit ~= unit) then return end
	unit = unit or self.unit
	
	local ai = self.AuraIndicators
	if ai then
		if ai.AuraList then
			if not(ai.Auras) then ai.Auras = {} else table.wipe(ai.Auras) end
			GetAuras(self, unit, UnitAura, ai, 'PLAYER')
		end
		
		for id,config in pairs(ai.AuraList) do
			typeFuncs[config.indicator](ai,id,config)
		end
	end
end

local Update = function(self,...)
	return UNIT_AURA(self, ...)
end

local ForceUpdate = function (element)
	return Update(element.__owner, 'ForceUpdate', element.__owner.unit)
end

local Enable = function (self)
	local ai = self.AuraIndicators
	if ai then
		ai.__owner = self
		ai.Auras = {}
		ai.Cache = {}
		ai.Timers = {}
		ai.Frames = ai.Frames or {}
		for k,v in pairs(ai.Frames) do v:Hide() end
		
		ai.ForceUpdate = ForceUpdate
		
		self:RegisterEvent('UNIT_AURA', UNIT_AURA)
	end
end

local Disable = function (self)
	local ai = self.AuraIndicators
	if ai then
		self:UnregisterEvent('UNIT_AURA', UNIT_AURA)
	end
end

oUF:AddElement('AuraIndicators', Update, Enable, Disable)