## Interface: 70000
## Version: 7.0.0

## Title: oUF: Taroven
## Notes: Taroven's amazingly hackish and shiny oUF layout.
## Author: Taroven

## OptionalDependencies: oUF, SharedMedia
## SavedVariables: oUF_TarovenDB
## X-oUF: Taroven

config.lua
Funcs.lua
Tags.lua

oUF\oUF.xml
oUF_Taroven.xml